resource "aws_vpc" "this" {
    cidr_block = "10.100.0.0/16"
    enable_dns_support = true
    enable_dns_hostnames = true
    enable_classiclink = false
    assign_generated_ipv6_cidr_block = false
    
    tags = {
        Name = "${var.environment} VPC"
        Environment = var.environment
    }
}

resource "aws_subnet" "one_a" {
    vpc_id = aws_vpc.this.id
    cidr_block = "10.100.0.0/24"
    availability_zone = "us-east-1a"
    map_public_ip_on_launch = false
    assign_ipv6_address_on_creation = false

    tags = {
        Name = "${var.environment} 1a Subnet"
        Environment = var.environment
    }
}

resource "aws_subnet" "one_b" {
    vpc_id = aws_vpc.this.id
    cidr_block = "10.100.1.0/24"
    availability_zone = "us-east-1b"
    map_public_ip_on_launch = false
    assign_ipv6_address_on_creation = false

    tags = {
        Name = "${var.environment} 1b Subnet"
        Environment = var.environment
    }
}

resource "aws_subnet" "one_c" {
    vpc_id = aws_vpc.this.id
    cidr_block = "10.100.2.0/24"
    availability_zone = "us-east-1c"
    map_public_ip_on_launch = false
    assign_ipv6_address_on_creation = false

    tags = {
        Name = "${var.environment} 1c Subnet"
        Environment = var.environment
    }
}

resource "aws_internet_gateway" "this" {
    vpc_id = aws_vpc.this.id

    tags = {
        Name = "${var.environment} Primary Internet Gateway"
        Environment = var.environment
    }
}

resource "aws_route_table" "this" {
    vpc_id = aws_vpc.this.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.this.id
    }

    tags = {
        Name = "${var.environment} Route Table"
        Environment = var.environment
    }
}

resource "aws_route_table_association" "one_a" {
    subnet_id = aws_subnet.one_a.id
    route_table_id = aws_route_table.this.id
}

resource "aws_route_table_association" "one_b" {
    subnet_id = aws_subnet.one_b.id
    route_table_id = aws_route_table.this.id
}

resource "aws_route_table_association" "one_c" {
    subnet_id = aws_subnet.one_c.id
    route_table_id = aws_route_table.this.id
}