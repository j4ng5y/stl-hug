output "vpc_id" {
    description = "The VPC ID"
    value = aws_vpc.this.id
}

output "subnet_1a_id" {
    description = "The Subnet ID for the 1a availability zone"
    value = aws_subnet.one_a.id
}

output "subnet_1b_id" {
    description = "The Subnet ID for the 1b availability zone"
    value = aws_subnet.one_b.id
}

output "subnet_1c_id" {
    description = "The Subnet ID for the 1c availability zone"
    value = aws_subnet.one_c.id
}