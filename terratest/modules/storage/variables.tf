variable "environment" {
    type = string
    description = "The envrionment to assign resources to"
}

variable "thing_key" {
    type = string
    description = "The key of the thing to upload"
}

variable "thing_source" {
    type = string
    description = "The source file to upload"
}