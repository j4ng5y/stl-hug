resource "aws_s3_bucket" "this" {
    bucket = "stuff.j4ng5y.dev"
    acl = "private"

    tags = {
        Name = "Stuff"
        Envrionment = var.environment
    }
}

resource "aws_s3_bucket_object" "thing" {
    bucket = aws_s3_bucket.this.id
    key = var.thing_key
    source = var.thing_source
}