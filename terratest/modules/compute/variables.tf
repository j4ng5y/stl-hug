variable "environment" {
    type = string
    description = "The envrionment to assign resources to"
}

variable "ami_id" {
    type = string
    description = "The AMI ID to launch"
}

variable "server_name" {
    type = string
    description = "what to call the server"
}

variable "subnet_id" {
    type = string
    description = "the subnet to launch the server into"
}

variable "vpc_id" {
    type = string
    description = "the vpc to launch the server into"
}