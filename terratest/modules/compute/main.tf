resource "aws_instance" "server" {
    ami = var.ami_id
    instance_type = "t2.micro"

    associate_public_ip_address = true

    subnet_id = var.subnet_id

    security_groups = [
        aws_security_group.server_sg.id
    ]

    tags = {
        Name = var.server_name
        Environment = var.environment
    }
}

resource "aws_security_group" "server_sg" {
    name = "${var.server_name}-security-group"
    description = "${var.server_name} security group"
    vpc_id = var.vpc_id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "${var.server_name}-security-group"
        Environment = var.environment
    }
}