module "networking" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/networking"
    environment = var.environment
}

module "storage" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/storage"
    environment = var.environment
    thing_key = "prod.txt"
    thing_source = "./prod.txt"
}

module "compute1a" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/compute"
    environment = var.environment
    ami_id = var.ami_id
    server_name = "prod_service_1a"
    subnet_id = module.networking.subnet_1a_id
    vpc_id = module.networking.vpc_id
}

module "compute1b" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/compute"
    environment = var.environment
    ami_id = var.ami_id
    server_name = "prod_service_1b"
    subnet_id = module.networking.subnet_1b_id
    vpc_id = module.networking.vpc_id
}

module "compute1c" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/compute"
    environment = var.environment
    ami_id = var.ami_id
    server_name = "prod_service_1c"
    subnet_id = module.networking.subnet_1c_id
    vpc_id = module.networking.vpc_id
}