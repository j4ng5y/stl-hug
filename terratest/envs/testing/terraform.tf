terraform {
    backend "s3" {
        bucket = "terraform-state.j4ng5y.dev"
        key = "state"
        region = "us-east-1"
        dynamodb_table = "terraform-state"
    }
}

provider "aws" {
    region = "us-east-1"
}