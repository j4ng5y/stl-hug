module "networking" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/networking"
    environment = var.environment
}

module "storage" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/storage"
    environment = var.environment
    thing_key = "test.txt"
    thing_source = "./test.txt"
}

output "bucket_name" {
    value = module.storage.bucket_name
}

module "compute1a" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/compute"
    environment = var.environment
    ami_id = var.ami_id
    server_name = "test_service_1a"
    subnet_id = module.networking.subnet_1a_id
    vpc_id = module.networking.vpc_id
}

output "onea_service_ip_address" {
    value = module.compute1a.service_public_ip_address
}

module "compute1b" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/compute"
    environment = var.environment
    ami_id = var.ami_id
    server_name = "test_service_1b"
    subnet_id = module.networking.subnet_1b_id
    vpc_id = module.networking.vpc_id
}

output "oneb_service_ip_address" {
    value = module.compute1b.service_public_ip_address
}

module "compute1c" {
    source = "git::https://gitlab.com/j4ng5y/stl-hug.git//terratest/modules/compute"
    environment = var.environment
    ami_id = var.ami_id
    server_name = "test_service_1c"
    subnet_id = module.networking.subnet_1c_id
    vpc_id = module.networking.vpc_id
}

output "onec_service_ip_address" {
    value = module.compute1c.service_public_ip_address
}