FROM golang:latest

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y \
     curl \
     unzip

RUN curl -sSLO https://releases.hashicorp.com/terraform/0.12.20/terraform_0.12.20_linux_amd64.zip \
 && curl -sSLO https://releases.hashicorp.com/packer/1.5.1/packer_1.5.1_linux_amd64.zip \
 && unzip -d /usr/bin terraform_0.12.20_linux_amd64.zip \
 && unzip -d /usr/bin packer_1.5.1_linux_amd64.zip

WORKDIR /terratest

COPY ["./testing", "./testing"]
COPY ["go.mod", "go.sum", "main.tf_test.go", "./"]

ENTRYPOINT [ "go", "test", "-v", "main.tf_test.go" ]