module gitlab.com/j4ng5y/stl-hug/terratest/envs/aws

go 1.13

require (
	github.com/aws/aws-sdk-go v1.29.0
	github.com/gruntwork-io/terratest v0.24.2
	golang.org/x/crypto v0.0.0-20200210222208-86ce3cb69678 // indirect
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
