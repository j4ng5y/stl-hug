package main

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	awsHelper "github.com/gruntwork-io/terratest/modules/aws"
	httpHelper "github.com/gruntwork-io/terratest/modules/http-helper"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
)

type AWSOptions struct {
	Region string
}

func TestService(t *testing.T) {
	terraformOpts := &terraform.Options{
		TerraformDir: "./testing",
		Vars: map[string]interface{}{
			"environment": fmt.Sprintf("testing-%s", random.UniqueId()),
			"ami_id":      fmt.Sprintf("ami-0eb4359a88e1c3ab7"),
		},
		Upgrade: true,
	}

	awsOptions := &AWSOptions{
		Region: "us-east-1",
	}

	defer terraform.Destroy(t, terraformOpts)

	terraform.InitAndApply(t, terraformOpts)

	bucketName := terraform.Output(t, terraformOpts, "bucket_name")
	oneAServiceIPAddr := terraform.Output(t, terraformOpts, "onea_service_ip_address")
	oneBServiceIPAddr := terraform.Output(t, terraformOpts, "oneb_service_ip_address")
	oneCServiceIPAddr := terraform.Output(t, terraformOpts, "onec_service_ip_address")
	c := awsHelper.NewS3Client(t, awsOptions.Region)
	objs, err := c.ListObjects(&s3.ListObjectsInput{
		Bucket: aws.String(bucketName),
	})
	if err != nil {
		t.Fail()
	}
	if len(objs.Contents) <= 0 {
		t.Fail()
	}

	httpHelper.HttpGetWithRetry(t, fmt.Sprintf("http://%s", oneAServiceIPAddr), nil, http.StatusOK, "successful", 3, 60*time.Second)
	httpHelper.HttpGetWithRetry(t, fmt.Sprintf("http://%s", oneBServiceIPAddr), nil, http.StatusOK, "successful", 3, 60*time.Second)
	httpHelper.HttpGetWithRetry(t, fmt.Sprintf("http://%s", oneCServiceIPAddr), nil, http.StatusOK, "successful", 3, 60*time.Second)
}
